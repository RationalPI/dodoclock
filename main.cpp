#include <QApplication>
#include "mainwindow.h"
#include <iostream>

int main(int argc, char *argv[]){
	QApplication a(argc, argv);
	MainWindow mw;
	mw.showMaximized();
	return a.exec();
}
