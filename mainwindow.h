#pragma once

#include <QPointer>
#include <QMainWindow>
#include <memory>

#include "qinput.h"


struct Macro_I{
	virtual void handleInputEvent(W::Input::Event e)=0;
	virtual ~Macro_I(){}
	virtual void doPause()=0;
	virtual bool active()=0;
};

struct MainWindow : public QMainWindow{
	MainWindow(QWidget *parent = nullptr);
private:
	QPointer<QWidget> centralwidget;
	QInput qi;
	std::vector<std::unique_ptr<Macro_I>> macros;
};
