#include "types.h"

#include <tuple>
#include <sstream>

bool Point::operator<(const Point& other) const {
	return std::tie(x,y) < std::tie(other.x,other.y);
}

std::string Point::toString(){
	std::stringstream ss;
	ss << "(x: " << x << " y: " << y << ")";
	return ss.str();
}
