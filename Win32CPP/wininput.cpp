#include "wininput.h"

#include <windows.h>
#include <winuser.h>

#include <thread>
#include <mutex>
#include <set>
#include <vector>
#include <array>
#include <unordered_map>
#include <type_traits>
#include <iostream>

using Ktype=std::underlying_type<W::Key>::type;


void W::pressKey(Key k){
	std::array<INPUT,2> inputs;
	ZeroMemory(&inputs.front(),sizeof(INPUT));
	ZeroMemory(&inputs.back(),sizeof(INPUT));

	switch (k) {
	case Key::mouse_LBUTTON:
	case Key::mouse_RBUTTON:
	case Key::mouse_MBUTTON:
	case Key::mouse_XBUTTON1:
	case Key::mouse_XBUTTON2:{
		inputs.front().type = INPUT_MOUSE;
		inputs.back().type = INPUT_MOUSE;

		switch (k){
		case Key::mouse_LBUTTON:{
			inputs.front().mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
			inputs.back().mi.dwFlags = MOUSEEVENTF_LEFTUP;
		}break;
		case Key::mouse_RBUTTON:{
			inputs.front().mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
			inputs.back().mi.dwFlags = MOUSEEVENTF_RIGHTUP;
		}break;
		case Key::mouse_MBUTTON:{
			inputs.front().mi.dwFlags = MOUSEEVENTF_MIDDLEDOWN;
			inputs.back().mi.dwFlags = MOUSEEVENTF_MIDDLEUP;
		}break;
		case Key::mouse_XBUTTON1:
		case Key::mouse_XBUTTON2:{
			inputs.front().mi.dwFlags = MOUSEEVENTF_XDOWN;
			inputs.back().mi.dwFlags = MOUSEEVENTF_XUP;
		}break;
		default:break;
		}

	}break;
	default:{
		inputs.front().type = INPUT_KEYBOARD;
		inputs.front().ki.wVk = static_cast<Ktype>(k);

		inputs.back().type = INPUT_KEYBOARD;
		inputs.back().ki.wVk = static_cast<Ktype>(k);
		inputs.back().ki.dwFlags = KEYEVENTF_KEYUP;
	}break;
	}

	SendInput(inputs.size(), inputs.data(), sizeof(INPUT));
}

void W::mouseMove(Point p){
	SetCursorPos(p.x,p.y);
}

void W::mouseMoveClick(Point p){
	mouseMove(p);
	pressKey(Key::mouse_LBUTTON);
}

W::Key W::waitKey() {
	while(true) {
		for (Ktype code = 0; code < static_cast<Ktype>(Key::F12); ++code) {
			auto k=GetAsyncKeyState(code);
			if((k & 0x8000) && (k & 0x0001)){ // The bitwise and selects the function behavior (look at doc)
				return Key(code);
			}
		}
		Sleep(1);
	}
}


std::string W::keyToString(Key k){
	static std::unordered_map<Key,std::string> m{
		{Key::mouse_LBUTTON,"mouse_LBUTTON"},
		{Key::mouse_RBUTTON,"mouse_RBUTTON"},
		{Key::mouse_CANCEL,"mouse_CANCEL"},
		{Key::mouse_MBUTTON,"mouse_MBUTTON"},
		{Key::mouse_XBUTTON1,"mouse_XBUTTON1"},
		{Key::mouse_XBUTTON2,"mouse_XBUTTON2"},
		{Key::BACK,"BACK"},
		{Key::TAB,"TAB"},
		{Key::CLEAR,"CLEAR"},
		{Key::RETURN,"RETURN"},
		{Key::SHIFT,"SHIFT"},
		{Key::CONTROL,"CONTROL"},
		{Key::MENU,"MENU"},
		{Key::PAUSE,"PAUSE"},
		{Key::CAPITAL,"CAPITAL"},
		{Key::KANA,"KANA"},
		{Key::HANGUEL,"HANGUEL"},
		{Key::HANGUL,"HANGUL"},
		{Key::IME_ON,"IME_ON"},
		{Key::JUNJA,"JUNJA"},
		{Key::FINAL,"FINAL"},
		{Key::HANJA,"HANJA"},
		{Key::KANJI,"KANJI"},
		{Key::IME_OFF,"IME_OFF"},
		{Key::ESCAPE,"ESCAPE"},
		{Key::CONVERT,"CONVERT"},
		{Key::NONCONVERT,"NONCONVERT"},
		{Key::ACCEPT,"ACCEPT"},
		{Key::MODECHANGE,"MODECHANGE"},
		{Key::SPACE,"SPACE"},
		{Key::PRIOR,"PRIOR"},
		{Key::NEXT,"NEXT"},
		{Key::END,"END"},
		{Key::HOME,"HOME"},
		{Key::LEFT,"LEFT"},
		{Key::UP,"UP"},
		{Key::RIGHT,"RIGHT"},
		{Key::DOWN,"DOWN"},
		{Key::SELECT,"SELECT"},
		{Key::PRINT,"PRINT"},
		{Key::EXECUTE,"EXECUTE"},
		{Key::SNAPSHOT,"SNAPSHOT"},
		{Key::INSERT,"INSERT"},
		{Key::DELETE_k,"keyDELETE"},
		{Key::HELP,"HELP"},
		{Key::num0,"num0"},
		{Key::num1,"num1"},
		{Key::num2,"num2"},
		{Key::num3,"num3"},
		{Key::num4,"num4"},
		{Key::num5,"num5"},
		{Key::num6,"num6"},
		{Key::num7,"num7"},
		{Key::num8,"num8"},
		{Key::num9,"num9"},
		{Key::A,"A"},
		{Key::B,"B"},
		{Key::C,"C"},
		{Key::D,"D"},
		{Key::E,"E"},
		{Key::F,"F"},
		{Key::G,"G"},
		{Key::H,"H"},
		{Key::I,"I"},
		{Key::J,"J"},
		{Key::K,"K"},
		{Key::L,"L"},
		{Key::M,"M"},
		{Key::N,"N"},
		{Key::O,"O"},
		{Key::P,"P"},
		{Key::Q,"Q"},
		{Key::R,"R"},
		{Key::S,"S"},
		{Key::T,"T"},
		{Key::U,"U"},
		{Key::V,"V"},
		{Key::W,"W"},
		{Key::X,"X"},
		{Key::Y,"Y"},
		{Key::Z,"Z"},
		{Key::LWIN,"LWIN"},
		{Key::RWIN,"RWIN"},
		{Key::APPS,"APPS"},
		{Key::SLEEP,"SLEEP"},
		{Key::NUMPAD0,"NUMPAD0"},
		{Key::NUMPAD1,"NUMPAD1"},
		{Key::NUMPAD2,"NUMPAD2"},
		{Key::NUMPAD3,"NUMPAD3"},
		{Key::NUMPAD4,"NUMPAD4"},
		{Key::NUMPAD5,"NUMPAD5"},
		{Key::NUMPAD6,"NUMPAD6"},
		{Key::NUMPAD7,"NUMPAD7"},
		{Key::NUMPAD8,"NUMPAD8"},
		{Key::NUMPAD9,"NUMPAD9"},
		{Key::MULTIPLY,"MULTIPLY"},
		{Key::ADD,"ADD"},
		{Key::SEPARATOR,"SEPARATOR"},
		{Key::SUBTRACT,"SUBTRACT"},
		{Key::keyDECIMAL,"keyDECIMAL"},
		{Key::DIVIDE,"DIVIDE"},
		{Key::F1,"F1"},
		{Key::F2,"F2"},
		{Key::F3,"F3"},
		{Key::F4,"F4"},
		{Key::F5,"F5"},
		{Key::F6,"F6"},
		{Key::F7,"F7"},
		{Key::F8,"F8"},
		{Key::F9,"F9"},
		{Key::F10,"F10"},
		{Key::F11,"F11"},
		{Key::F12,"F12"},
		{Key::F13,"F13"},
		{Key::F14,"F14"},
		{Key::F15,"F15"},
		{Key::F16,"F16"},
		{Key::F17,"F17"},
		{Key::F18,"F18"},
		{Key::F19,"F19"},
		{Key::F20,"F20"},
		{Key::F21,"F21"},
		{Key::F22,"F22"},
		{Key::F23,"F23"},
		{Key::F24,"F24"},
		{Key::NUMLOCK,"NUMLOCK"},
		{Key::SCROLL,"SCROLL"},
		{Key::LSHIFT,"LSHIFT"},
		{Key::RSHIFT,"RSHIFT"},
		{Key::LCONTROL,"LCONTROL"},
		{Key::RCONTROL,"RCONTROL"},
		{Key::LMENU,"LMENU"},
		{Key::RMENU,"RMENU"},
		{Key::BROWSER_BACK,"BROWSER_BACK"},
		{Key::BROWSER_FORWARD,"BROWSER_FORWARD"},
		{Key::BROWSER_REFRESH,"BROWSER_REFRESH"},
		{Key::BROWSER_STOP,"BROWSER_STOP"},
		{Key::BROWSER_SEARCH,"BROWSER_SEARCH"},
		{Key::BROWSER_FAVORITES,"BROWSER_FAVORITES"},
		{Key::BROWSER_HOME,"BROWSER_HOME"},
		{Key::VOLUME_MUTE,"VOLUME_MUTE"},
		{Key::VOLUME_DOWN,"VOLUME_DOWN"},
		{Key::VOLUME_UP,"VOLUME_UP"},
		{Key::MEDIA_NEXT_TRACK,"MEDIA_NEXT_TRACK"},
		{Key::MEDIA_PREV_TRACK,"MEDIA_PREV_TRACK"},
		{Key::MEDIA_STOP,"MEDIA_STOP"},
		{Key::MEDIA_PLAY_PAUSE,"MEDIA_PLAY_PAUSE"},
		{Key::LAUNCH_MAIL,"LAUNCH_MAIL"},
		{Key::LAUNCH_MEDIA_SELECT,"LAUNCH_MEDIA_SELECT"},
		{Key::LAUNCH_APP1,"LAUNCH_APP1"},
		{Key::LAUNCH_APP2,"LAUNCH_APP2"},
		{Key::OEM_1,"OEM_1"},
		{Key::OEM_PLUS,"OEM_PLUS"},
		{Key::OEM_COMMA,"OEM_COMMA"},
		{Key::OEM_MINUS,"OEM_MINUS"},
		{Key::OEM_PERIOD,"OEM_PERIOD"},
		{Key::OEM_2,"OEM_2"},
		{Key::OEM_3,"OEM_3"},
		{Key::OEM_4,"OEM_4"},
		{Key::OEM_5,"OEM_5"},
		{Key::OEM_6,"OEM_6"},
		{Key::OEM_7,"OEM_7"},
		{Key::OEM_8,"OEM_8"},
		{Key::OEM_102,"OEM_102"},
		{Key::PROCESSKEY,"PROCESSKEY"},
		{Key::PACKET,"PACKET"},
		{Key::ATTN,"ATTN"},
		{Key::CRSEL,"CRSEL"},
		{Key::EXSEL,"EXSEL"},
		{Key::EREOF,"EREOF"},
		{Key::PLAY,"PLAY"},
		{Key::ZOOM,"ZOOM"},
		{Key::NONAME,"NONAME"},
		{Key::PA1,"PA1"},
		{Key::OEM_CLEAR,"OEM_CLEAR"},
	};
	return m[k];
}

namespace W{
struct Statics{
	std::set<Input*> instances;
	std::mutex instancesM;
	std::thread hoockingThread;
	HHOOK keyboardHoockHandle;
	HHOOK mouseHoockHandle;
	bool abortRequested;
	std::unordered_map<W::Key,bool> isDown;
}s;

LRESULT keyboardHookFuction(int nCode, WPARAM wParam, LPARAM lParam){
	if(!s.abortRequested && s.instancesM.try_lock()){
		if (nCode == HC_ACTION){
			auto key=Key(((KBDLLHOOKSTRUCT*)lParam)->vkCode);
			switch (wParam){
			case WM_KEYDOWN:
			case WM_SYSKEYDOWN:{
				if(!s.isDown[key]){
					for (auto& instance : s.instances) {
						instance->cb_({Input::Event::KeyPress({key,Input::Event::KeyPress::Type::KeyDown}),std::nullopt});
					}
					s.isDown[key]=true;
				}
			}break;
			case WM_KEYUP:
			case WM_SYSKEYUP:{
				for (auto& instance : s.instances) {
					instance->cb_({Input::Event::KeyPress({key,Input::Event::KeyPress::Type::KeyUp}),std::nullopt});
				}
				s.isDown[key]=false;
			}break;
			}
		}
		s.instancesM.unlock();
	}

	return CallNextHookEx(NULL, nCode, wParam, lParam);// propagate the event (return 1 if the event is handled)
}
LRESULT mouseHookFuction(int nCode, WPARAM wParam, LPARAM lParam){
	if (auto pMouseStruct = (MSLLHOOKSTRUCT*)lParam){
		Key k=Key::ESCAPE;/*just used a flag for no button pressed*/
		Input::Event::KeyPress::Type et;
		switch (wParam) {
		case WM_LBUTTONUP		: k=Key::mouse_LBUTTON ; et=Input::Event::KeyPress::Type::KeyUp  ; break;
		case WM_LBUTTONDOWN	: k=Key::mouse_LBUTTON ; et=Input::Event::KeyPress::Type::KeyDown; break;

		case WM_RBUTTONUP		: k=Key::mouse_RBUTTON ; et=Input::Event::KeyPress::Type::KeyUp  ; break;
		case WM_RBUTTONDOWN	: k=Key::mouse_RBUTTON ; et=Input::Event::KeyPress::Type::KeyDown; break;

		case WM_MBUTTONUP		: k=Key::mouse_MBUTTON ; et=Input::Event::KeyPress::Type::KeyUp  ; break;
		case WM_MBUTTONDOWN	: k=Key::mouse_MBUTTON ; et=Input::Event::KeyPress::Type::KeyDown; break;

		case WM_XBUTTONUP		: k=Key::mouse_XBUTTON1; et=Input::Event::KeyPress::Type::KeyUp  ; break;
		case WM_XBUTTONDOWN	: k=Key::mouse_XBUTTON1; et=Input::Event::KeyPress::Type::KeyDown; break;
		}

		struct DWSplit{ WORD hight,low;};
		if(k==Key::mouse_XBUTTON1 && ((DWSplit*)&pMouseStruct->mouseData)->low==2){
			k=Key::mouse_XBUTTON2;
		}
		if(!s.abortRequested && s.instancesM.try_lock()){
			if(k!=Key::ESCAPE){
				Input::Event event={Input::Event::KeyPress({k,et}),Point({pMouseStruct->pt.x,pMouseStruct->pt.y})};

				if(et==Input::Event::KeyPress::Type::KeyUp){
					for (auto& instance : s.instances) {
						instance->cb_(event);
					}
					s.isDown[k]=false;
				}else{
					if(!s.isDown[k]){
						for (auto& instance : s.instances) {
							instance->cb_(event);
						}
						s.isDown[k]=true;
					}
				}

			}else{
				for (auto& instance : s.instances) {
					instance->cb_({std::nullopt,Point({pMouseStruct->pt.x,pMouseStruct->pt.y})});
				}
			}

			s.instancesM.unlock();
		}
	}

	return CallNextHookEx(NULL, nCode, wParam, lParam);// propagate the event (return 1 if the event is handled)
}
}

const std::unordered_map<W::Key, bool>& W::Input::globalState(){
	return s.isDown;
}

bool W::Input::keyPressed(W::Key k){
	return s.isDown.count(k) &&  s.isDown.at(k);
}

W::Input::Input(std::function<void(Event)> cb):cb_(cb){
	s.instancesM.lock();
	if(!s.instances.size()){
		s.instances.insert(this);
		s.abortRequested=false;

		s.hoockingThread=std::thread([=](){

			s.keyboardHoockHandle = SetWindowsHookEx(WH_KEYBOARD_LL, keyboardHookFuction, 0, 0);
			s.mouseHoockHandle = SetWindowsHookEx(WH_MOUSE_LL, mouseHookFuction, 0, 0);

			UINT_PTR timedMessageHandle = SetTimer(nullptr, 0, 2000, nullptr); // used as a tiemout for GetMessage
			MSG msg;
			while (GetMessage(&msg, nullptr, 0, 0) && !s.abortRequested) { //this while loop keeps the hook
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			KillTimer(NULL, timedMessageHandle);

			UnhookWindowsHookEx(s.keyboardHoockHandle);
			UnhookWindowsHookEx(s.mouseHoockHandle);
		});
	}else{
		s.instances.insert(this);
	}
	s.instancesM.unlock();
}

W::Input::~Input(){
	s.instancesM.lock();
	if(s.instances.count(this)){
		s.instances.erase(this);
	}
	if(!s.instances.size()){
		s.abortRequested=true;
		s.hoockingThread.join();
	}
	s.instancesM.unlock();
}
