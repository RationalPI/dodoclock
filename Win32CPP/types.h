#pragma once
#include <string>

struct Point{
	int x,y;
	bool operator<(const Point& other) const;
	std::string toString();
};
