#include "mainwindow.h"

#include <QEvent>
#include <QApplication>
#include <QStyle>
#include <QMenu>
#include <QTime>
#include <QGraphicsDropShadowEffect>
#include <QThread>
#include <QSpinBox>
#include <QScreen>
#include <QPainter>
#include <QPointer>
#include <QCheckBox>
#include <QVBoxLayout>
#include <QLabel>
#include <QSystemTrayIcon>
#include <QTimer>
#include <QPushButton>
#include <stdint.h>

#include <iostream>
#include <Win32CPP/winpixel.h>

template<class Widget>
QPointer<Widget> allocateToLayout(QPointer<QBoxLayout> ly,Widget* wg){
	ly->addWidget(wg);
	return wg;
}


struct Alarm{
	QTime time;
	Qt::DayOfWeek day;

	uint64_t secTo()const {
		constexpr auto secPerDay=24*60*60;
		auto t = QTime::currentTime();

		auto me=time.hour()*60*60+time.minute()*60+time.second();
		auto now=t.hour()*60*60+t.minute()*60+t.second();
		auto ret=me-now;
		ret+=secPerDay*(day-QDate::currentDate().dayOfWeek());
		ret+=7*secPerDay;
		ret=ret%(7*secPerDay);
		return ret;
	}

	bool operator <(const Alarm& other)const {
		return secTo()<other.secTo();
	}

	QString timeLeftString()const {
		auto s=secTo();

		auto hours=std::ceil(s/(60*60));
		uint32_t minutes=std::max(s/60.-hours*60,1.);
		return QString::number(hours)+":"+(QString::number(minutes).size()==1?"0"+QString::number(minutes):QString::number(minutes));
	}
};

std::vector<Alarm> alarms{
	{QTime(12,30),Qt::DayOfWeek::Tuesday},

	{QTime(8,30),Qt::DayOfWeek::Monday},
	{QTime(8,30),Qt::DayOfWeek::Tuesday},
	{QTime(9,15),Qt::DayOfWeek::Wednesday},
	{QTime(8,30),Qt::DayOfWeek::Thursday},
	{QTime(8,30),Qt::DayOfWeek::Friday},
	{QTime(10,30),Qt::DayOfWeek::Saturday},
	{QTime(10,30),Qt::DayOfWeek::Sunday}
};

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent){
	setStyleSheet("background:transparent;");
	setAttribute(Qt::WA_TranslucentBackground);
	setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint/*|Qt::ToolTip*/);
	centralwidget = new QWidget(this);
	setCentralWidget(centralwidget);

	connect(&qi,&QInput::handleEventThreadSafe,[=](W::Input::Event event){
		for (auto& macro : macros) {
			macro->handleInputEvent(event);
		}
		//		if (event.keyPress) std::cout << W::keyToString(event.keyPress->key) <<  std::endl;
		//		if (e.pos) std::cout << e.pos->toString() << std::endl;
	});

	auto appContextMenu=[this](){
		QMenu m;
		connect(m.addAction("Exit"),&QAction::triggered,[]{QApplication::quit();});
		connect(m.addAction("Configure"),&QAction::triggered,[]{

		});
		m.exec(QCursor::pos());
	};

	/*tray icone*/{
		auto trayIcon=new QSystemTrayIcon(this);
		trayIcon->setIcon(QApplication::style()->standardIcon(QStyle::SP_MessageBoxCritical));
		trayIcon->setToolTip("Clock");
		trayIcon->setVisible(true);
		connect(trayIcon,&QSystemTrayIcon::activated,appContextMenu);
	}
	/*clock*/{
		QFont font;{
			font.setFamily(QString::fromUtf8("Gadugi"));
		}

		QLabel* currentTime = new QLabel(centralwidget);

		currentTime->setStyleSheet("QLabel { color : cyan ; background-color : rgba(0, 0, 0, 0.4); }");
		currentTime->setGeometry(0,-8,105,50);
		currentTime->setAlignment(Qt::AlignCenter);


		font.setPointSize(30);
		currentTime->setFont(font);

		currentTime->setContextMenuPolicy(Qt::ContextMenuPolicy::CustomContextMenu);
		connect(currentTime,&QWidget::customContextMenuRequested,appContextMenu);

		QLabel* CoutDown = new QLabel(centralwidget);
		CoutDown->setStyleSheet("QLabel { color : cyan ; background-color : rgba(0, 0, 0, 0.4); }");
		CoutDown->setGeometry(0,45,105,50);
		CoutDown->setAlignment(Qt::AlignCenter);
		font.setPointSize(24);
		CoutDown->setFont(font);



		/*timer updating and drawing*/{
			auto updateRender=[currentTime,CoutDown]{
				currentTime->setText(
							QString("<font color=\"cyan\">%1</font>").arg(QTime::currentTime().toString("hh:mm"))
							);
				std::sort(alarms.begin(),alarms.end());
				CoutDown->setText(
							QString("<font color=\"cyan\">%1</font>").arg(alarms.front().timeLeftString())
							);
			};

			updateRender();

			auto t=new QTimer(centralwidget);
			t->setSingleShot(false);
			t->setInterval(1000*30);
			connect(t,&QTimer::timeout,updateRender);
			t->start();
		}
	}
}
