#include "qinput.h"
#include <qcursor.h>
#include <iostream>

QInput::QInput()
	:kh([this](W::Input::Event e){ emit handleEvent(e);})
{
connect(this,&QInput::handleEvent,this,[this](W::Input::Event e){
	emit handleEventThreadSafe(e);
},Qt::ConnectionType::QueuedConnection);
}

Point QInput::currentPos(){
	auto p=QCursor::pos();
	return {p.x(),p.y()};
}
